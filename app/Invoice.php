<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = [
        "id",
        "customer_name",
        "customer_email",
        "customer_phone",
        "customer_no",
        "num",
        "gross_total",
        "discount_total",
        "tax_total",
        "tax_rate",
        "discount_rate",
        "net_total",
        "amount_recieved",
        "balance",
        "created_at",
        "is_void",
        "app_id",
        "staff_id"
    ];



    public function products(): BelongsToMany
    {
        return $this->belongsToMany("App\Product");
    }

    public function listProducts(): HasMany
    {
        return $this->hasMany("App\InvoiceProduct");
    }
    public function app(): BelongsTo
    {
        return $this->belongsTo("App\App");
    }

    public function staff(): BelongsTo
    {
        return $this->belongsTo("App\Staff");
    }

    public function receipts(): HasMany
    {
        return $this->hasMany("App\Receipt");
    }

    public function company(): HasOne
    {
        return $this->hasOne("App\Setting", "app_id", "app_id");
    }
}
