<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipttype extends Model
{

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = "receipt_type";
    protected $fillable = [
        "id",
        "name",
        "created_at",
        "updated_at",
        "deleted_at",
        "Attribute1",
    ];

    public function receipts(): HasMany
    {
        return $this->hasMany("App\\Receipt");
    }
}
