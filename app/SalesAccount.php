<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesAccount extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = "sales_account";

    protected $fillable = [
        "id",
        "invoice_id",
        "sales_total",
        "is_void",
        "void_reason",
        "replace_void",
        "created_at",
        "updated_at",
        "deleted_at",
        "staff_id",
        "app_id",
    ];

    public function invoice(): BelongsTo
    {
        return $this->belongsTo("App\\Invoice");
    }
}
