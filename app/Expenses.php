<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expenses extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "num",
        "amount",
        "description",
        "created_at",
        "updated_at",
        "deleted_at",
        "expensetype_id",
        "app_id",
    ];

    public function expenseType(): BelongsTo
    {
        return $this->belongsTo("App\\ExpenseType", "expensetype_id", "id");
    }

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }
}
