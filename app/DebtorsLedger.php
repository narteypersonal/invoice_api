<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebtorsLedger extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = "debtors_ledger";
    protected $fillable = [
        "id",
        "customer_no",
        "total_sale",
        "cash_reciept",
        "balance_bd",
        "is_void",
        "void_reason",
        "replace_void",
        "created_at",
        "updated_at",
        "deleted_at",
        "staff_id",
        "app_id",
    ];

    public function invoices(): HasMany
    {
        return $this->hasMany("App\\Invoice", 'customer_no', 'customer_no');
    }
}
