<?php
namespace App\Exceptions;

use Exception;

class StaffException extends Exception
{
    public function __construct()
    {
        parent::__construct();
    }
}