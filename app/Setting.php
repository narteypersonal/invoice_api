<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = [
        "id",
        "company_name",
        "company_email",
        "company_phone",
        "company_address",
        "logo_url",
        "app_id",
        "created_at",
        "updated_at",
    ];

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }
}
