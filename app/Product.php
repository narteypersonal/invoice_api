<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $attribute = [
        "unit_price",
        "sub_total",
        "quantity"
    ];

    protected $fillable = [
        "id",
        "name",
        "price",
        "unit_id",
        "quantity",
        "discount_rate",
        "tax_rate",
        "created_at",
        "updated_at",
        "staff_id",
        "app_id"
    ];

    public function invoices(): BelongsToMany
    {
        return $this->belongsToMany("App\\Invoice");
    }

    public function unit(): BelongsTo
    {
        return $this->belongsTo("App\\Unit");
    }

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }

    public function staff(): BelongsTo
    {
        return $this->belongsTo("App\\Staff");
    }
}
