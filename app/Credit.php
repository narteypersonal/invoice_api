<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Credit extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "number",
        "description",
        "creditor_id",
        "debit",
        "credit",
        "created_at",
        "updated_at",
        "is_void",
        "replace_void",
        "deleted_at",
        "app_id",
    ];

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }

    public function creditor(): BelongsTo
    {
        return $this->belongsTo("App\\Creditor");
    }
}
