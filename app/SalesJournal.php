<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesJournal extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = "sales_journal";
    protected $fillable = [
        "id",
        "invoice_id",
        "arears",
        "is_void",
        "void_reason",
        "replace_void",
        "created_at",
        "updated_at",
        "deleted_at",
        "staff_id",
        "app_id",
    ];

    public function invoice(): BelongsTo
    {
        return $this->belongsTo("App\\Invoice");
    }

    public function salesJournal(): HasOne
    {
        return $this->hasOne("App\\SalesJournal");
    }

    public function replacedSalesJournal(): BelongsTo
    {
        return $this->belongsTo("App\\SalesJournal");
    }
}
