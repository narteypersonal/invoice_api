<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cashbook extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "description",
        "folio_id",
        "folio_name",
        "folio_number",
        "debit",
        "credit",
        "app_id",
        "updated_at",
        "created_at",
        "deleted_at",
    ];

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }
}
