<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PurchaseInvoiceNumber extends Model
{
	protected $fillable = [
		"id",
		"number",
		"alpha_index",
		"app_id",
		"created_at",
		"updated_at",
		"deleted_at",
	];

	public function app():HasOne
	{
		return $this->hasOne("App\\App"); 
	}

}