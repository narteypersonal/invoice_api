<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpensesNumber extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = "expenses_number";
    protected $fillable = [
        "prefix",
        "number",
        "alpha_index",
        "app_id",
        "created_at",
        "updated_at"
    ];

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }
}
