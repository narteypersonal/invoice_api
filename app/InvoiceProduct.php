<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceProduct extends Model
{

    // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    protected $table = "invoice_product";

    protected $fillable = [
        "id",
        "product_id",
        "invoice_id",
        "quantity",
        "unit_price",
        "sub_total",
        "discount_rate",
        "tax_rate",
        "description",
        "created_at",
        "staff_id",
        "app_id"
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo("App\\Product");
    }

    public function invoice(): BelongsTo
    {
        return $this->belongsTo("App\\Invoice");
    }

    public function unit(): BelongsTo
    {
        return $this->belongsTo("App\\Unit");
    }

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }

    public function staff(): BelongsTo
    {
        return $this->belongsTo("App\\Staff");
    }
}
