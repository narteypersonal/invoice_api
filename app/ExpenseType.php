<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenseType extends Model
{

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $table = "expenses_type";

    protected $fillable = [
        "id",
        "name",
        "description",
        "created_at",
        "updated_at",
        "app_id"
    ];

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }

    public function expenses(): HasMany
    {
        return $this->hasMany("App\\Expenses", "expensetype_id");
    }
}
