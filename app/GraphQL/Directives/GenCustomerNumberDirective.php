<?php

namespace App\GraphQL\Directives;

use Closure;
use App\CustomerNumber;
use App\Invoice;
use Illuminate\Support\Arr;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;


class GenCustomerNumberDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name(): string
    {
        return 'genCustomerNumber';
    }

    /**
     * Resolve the field directive.
     *
     * @param  \Nuwave\Lighthouse\Schema\Values\FieldValue  $value
     * @param  \Closure  $next
     * @return \Nuwave\Lighthouse\Schema\Values\FieldValue
     *
     * @throws \Nuwave\Lighthouse\Exceptions\DirectiveException
     */
    public function handleField(FieldValue $value, Closure $next): FieldValue
    {

        $previousResolver = $value->getResolver();

        return $next(
            $value->setResolver(
                function ($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($previousResolver) {
                    $key = $this->directiveArgValue('key', "customer_no");
                    $alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

                    $customer_number = CustomerNumber::where('app_id', $context->user->app_id)->first();
                    $prefix = $customer_number->prefix;
                    $alpha_index = $customer_number->alpha_index;
                    $alpha = $alphabets[($alpha_index - 1)];
                    $num = strval($customer_number->number);
                    $num = "000" . $num;
                    $num = substr($num, strlen($num) - 3);

                    $customer_num = $prefix . $alpha . $num . date("Y");

                    // dd($customer_num);
                    if (!strlen($args[$key]) > 0 || !Invoice::where('customer_no', $args[$key])->where('app_id', $context->user->app_id)->first()) {
                        $args[$key] = $customer_num;
                        if ($num === 100) {
                            $alpha_index = ($customer_number->alpha_index % 26) + 1;
                        }
                        $customer_number->update(['number' => ($num % 100) + 1, 'alpha_index' => $alpha_index]);
                    }

                    // dd($args);
                    return $previousResolver(
                        $rootValue,
                        $args,
                        $context,
                        $resolveInfo
                    );
                }
            )
        );
    }
}
