<?php

namespace App\GraphQL\Directives;

use Closure;
use Illuminate\Support\Arr;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;



class EstimateInvoiceDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name(): string
    {
        return 'estimateInvoice';
    }

    /**
     * Resolve the field directive.
     *
     * @param  \Nuwave\Lighthouse\Schema\Values\FieldValue  $value
     * @param  \Closure  $next
     * @return \Nuwave\Lighthouse\Schema\Values\FieldValue
     *
     * @throws \Nuwave\Lighthouse\Exceptions\DirectiveException
     */
    public function handleField(FieldValue $value, Closure $next): FieldValue
    {

        $previousResolver = $value->getResolver();

        return $next(
            $value->setResolver(
                function ($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($previousResolver) {
                    $app = $context->user->app;
                    // dd($args["data"]["products"]);

                    $args['gross_total'] = 0;
                    $taxTotal = 0;
                    foreach ($args["products"] as $key => $product) {
                        # code...
                        $prod = $app->products->where('id', $product['product_id'])->where('quantity', '>', 0)->first();
                        if (!$prod)
                            throw new ProductError("Product Error", "Product not found");

                        $product['sub_total'] = $prod->price * $product['quantity'];
                        $product['unit_price'] = $prod->price;

                        $product['discount_rate'] = $args['discount_rate'];
                        $product['tax_rate'] = $args['tax_rate'];

                        $args['gross_total'] += $product['sub_total'];
                        $args['products'][$key] = $product;
                    }
                    $args['amount_recieved'] = $args['amount_recieved'] ?? 0;
                    $args['tax_total'] = $args['gross_total'] * $args['tax_rate'] / 100;
                    $args['gross_total'] += $args['tax_total'];
                    $discount_total = $args['gross_total'] * $args['discount_rate'] / 100;
                    $args['discount_total'] = $discount_total;
                    $args['net_total'] = $args['gross_total'] - $discount_total;
                    $args['balance'] =  $args['gross_total'] - $args['amount_recieved'];

                    return $previousResolver(
                        $rootValue,
                        $args,
                        $context,
                        $resolveInfo
                    );
                }
            )
        );
    }
}
