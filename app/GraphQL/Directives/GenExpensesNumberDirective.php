<?php

namespace App\GraphQL\Directives;

use Closure;
use App\ExpensesNumber;
use App\Expenses;
use Illuminate\Support\Arr;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;


class GenExpensesNumberDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name(): string
    {
        return 'genExpensesNumber';
    }

    /**
     * Resolve the field directive.
     *
     * @param  \Nuwave\Lighthouse\Schema\Values\FieldValue  $value
     * @param  \Closure  $next
     * @return \Nuwave\Lighthouse\Schema\Values\FieldValue
     *
     * @throws \Nuwave\Lighthouse\Exceptions\DirectiveException
     */
    public function handleField(FieldValue $value, Closure $next): FieldValue
    {

        $previousResolver = $value->getResolver();

        return $next(
            $value->setResolver(
                function ($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($previousResolver) {
                    $key = $this->directiveArgValue('key', "num");
                    $alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

                    $expenses_number = ExpensesNumber::where('app_id', $context->user->app_id)->first();
                    $prefix = $expenses_number->prefix;
                    $alpha_index = $expenses_number->alpha_index;
                    $alpha = $alphabets[($alpha_index - 1)];
                    $num = strval($expenses_number->number);
                    $num = "000" . $num;
                    $num = substr($num, strlen($num) - 3);

                    $expenses_num = $prefix . $alpha . $num . date("Y");

                    // dd($expenses_num);
                    // dd();
                    $args[$key] = $args[$key] ?? "";
                    if ((!strlen($args[$key]) > 0 || !Expenses::where('num', $args[$key])->where('app_id', $context->user->app_id)->first())) {
                        $args[$key] = $expenses_num;
                        if ($num === 100) {
                            $alpha_index = ($expenses_number->alpha_index % 26) + 1;
                        }
                    } else {
                        $args[$key] = $expenses_num;
                    }

                    // dd($args);
                    return $previousResolver(
                        $rootValue,
                        $args,
                        $context,
                        $resolveInfo
                    );
                }
            )
        );
    }
}
