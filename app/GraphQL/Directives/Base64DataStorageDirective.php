<?php

namespace App\GraphQL\Directives;

use Closure;
use Illuminate\Support\Arr;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;



class Base64DataStorageDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name(): string
    {
        return 'base64DataStorage';
    }

    /**
     * Resolve the field directive.
     *
     * @param  \Nuwave\Lighthouse\Schema\Values\FieldValue  $value
     * @param  \Closure  $next
     * @return \Nuwave\Lighthouse\Schema\Values\FieldValue
     *
     * @throws \Nuwave\Lighthouse\Exceptions\DirectiveException
     */
    public function handleField(FieldValue $value, Closure $next): FieldValue
    {

        $previousResolver = $value->getResolver();

        return $next(
            $value->setResolver(
                function ($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($previousResolver) {
                    $data = $this->directiveArgValue('data', "fileData");
                    $uri = $this->directiveArgValue('uri', "logo_uri");
                    if (
                        $args[$data] == "" ||
                        !($explo = explode(',', $args[$data])) ||
                        !($ext = explode(";", explode('/', $explo[0])[1])[0])
                    )
                        return;

                    $path =  $path = time() . '.' . $ext;
                    // dd($path);
                    if (isset($args[$data]) && ($image = base64_decode($explo[1]))) {
                        file_put_contents(public_path('images') . '/' . $path, $image);
                        // dd(Arr::add($args, "path", "images/" . $path));
                        // dd($ext);
                        return $previousResolver(
                            $rootValue,
                            Arr::add($args, $uri, "images/" . $path),
                            $context,
                            $resolveInfo
                        );
                    }
                }
            )
        );
    }
}
