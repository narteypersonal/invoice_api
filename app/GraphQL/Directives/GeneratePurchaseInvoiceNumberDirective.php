<?php

namespace App\GraphQL\Directives;

use Closure;
use App\PurchaseInvoiceNumber;
use Illuminate\Support\Arr;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;



class GeneratePurchaseInvoiceNumberDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name(): string
    {
        return 'generatePurchaseInvoiceNumber';
    }

    /**
     * Resolve the field directive.
     *
     * @param  \Nuwave\Lighthouse\Schema\Values\FieldValue  $value
     * @param  \Closure  $next
     * @return \Nuwave\Lighthouse\Schema\Values\FieldValue
     *
     * @throws \Nuwave\Lighthouse\Exceptions\DirectiveException
     */
    public function handleField(FieldValue $value, Closure $next): FieldValue
    {

        $previousResolver = $value->getResolver();

        return $next(
            $value->setResolver(
                function ($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($previousResolver) {
                    $key = $this->directiveArgValue('key', "number");
                    $alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
                    $number = PurchaseInvoiceNumber::where('app_id', $context->user->app_id)->first();

                    $prefix = $number->prefix;
                    $alpha = $alphabets[($number->alpha_index - 1)];
                    $num = strval($number->number);
                    $num = "000" . $num;
                    $num = substr($num, strlen($num) - 3);

                    $invoice_num = $prefix . $alpha . $num . date("Y");
                    // dd($invoice_num);
                    return $previousResolver(
                        $rootValue,
                        Arr::add($args, $key, $invoice_num),
                        $context,
                        $resolveInfo
                    );
                }
            )
        );
    }
}
