<?php

namespace App\GraphQL\Directives;

use Closure;
use App\CreditNumber;
use App\Credit;
use Illuminate\Support\Arr;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;


class GenCreditNumberDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name(): string
    {
        return 'genCreditNumber';
    }

    /**
     * Resolve the field directive.
     *
     * @param  \Nuwave\Lighthouse\Schema\Values\FieldValue  $value
     * @param  \Closure  $next
     * @return \Nuwave\Lighthouse\Schema\Values\FieldValue
     *
     * @throws \Nuwave\Lighthouse\Exceptions\DirectiveException
     */
    public function handleField(FieldValue $value, Closure $next): FieldValue
    {

        $previousResolver = $value->getResolver();

        return $next(
            $value->setResolver(
                function ($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($previousResolver) {
                    $key = $this->directiveArgValue('key', "number");
                    $alphabets = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

                    $credit_number = CreditNumber::where('app_id', $context->user->app_id)->first();
                    $prefix = $credit_number->prefix;
                    $alpha_index = $credit_number->alpha_index;
                    $alpha = $alphabets[($alpha_index - 1)];
                    $num = strval($credit_number->number);
                    $num = "000" . $num;
                    $num = substr($num, strlen($num) - 3);

                    $credit_num = $prefix . $alpha . $num . date("Y");


                    $args[$key] = $args[$key] ?? "";
                    if (!strlen($args[$key]) > 0 || !Credit::where('number', $args[$key])->where('app_id', $context->user->app_id)->first()) {
                        $args[$key] = $credit_num;
                        if ($num === 100) {
                            $alpha_index = ($credit_number->alpha_index % 26) + 1;
                        }
                        $credit_number->update(['number' => ($num % 100) + 1, 'alpha_index' => $alpha_index]);
                    }

                    // dd($args);
                    return $previousResolver(
                        $rootValue,
                        $args,
                        $context,
                        $resolveInfo
                    );
                }
            )
        );
    }
}
