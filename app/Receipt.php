<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = [
        "id",
        "description",
        "amount",
        "depositor",
        "invoice_id",
        'customer_no',
        "staff_id",
        "app_id",
        "amount",
        "amount_received",
        "amount_changed",
        "receipttype_id",
        "is_void",
        "created_at",
        "updated_at",
        "deleted_at",
        "invoice_trigger"
    ];

    public function receipttype(): HasOne
    {
        return $this->hasOne("App\\Receipttype", "id", "receipttype_id");
    }

    public function invoice(): BelongsTo
    {
        return $this->belongsTo("App\\Invoice");
    }
}
