<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Creditor extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = [
        "id",
        "name",
        "description",
        "contact",
        "created_at",
        "updated_at",
        "deleted_at",
        "app_id",
    ];

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }

    public function credits(): HasMany
    {
        return $this->hasMany("App\\Credit");
    }
}
