<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseInvoice extends Model
{
    use SoftDeletes;
    protected $fillable = [
        "id",
        "name",
        "amount",
        "amount_paid",
        "balance_bd",
        "created_at",
        "updated_at",
        "deleted_at",
        "app_id",
        "creditor_id",
        "number",
        "is_served",
        "is_void",
        "carriage_cost",
    ];

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }

    public function creditor(): BelongsTo
    {
        return $this->belongsTo("App\\Creditor");
    }
}
