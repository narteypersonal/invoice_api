<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        "id",
        "quantity",
        "product_id",
        "is_void",
        "created_at",
        "updated_at",
        "deleted_at",
        "staff_id",
        "app_id"
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo("App\\Product");
    }

    public function app(): BelongsTo
    {
        return $this->belongsTo("App\\App");
    }

    public function staff(): BelongsTo
    {
        return $this->belongsTo("App\\Staff");
    }
}
