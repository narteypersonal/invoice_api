<?php

use Illuminate\Database\Seeder;

class ReceipttypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('receipt_type')->insert([
            [
                'name' => 'part payment',
                'created_at' => new DateTime('now'), 'updated_at' => new DateTime('now')
            ],
            [
                'name' => 'full payment',
                'created_at' => new DateTime('now'), 'updated_at' => new DateTime('now')
            ],
            [
                'name' => 'initial payment',
                'created_at' => new DateTime('now'), 'updated_at' => new DateTime('now')
            ],
            [
                'name' => 'final payment',
                'created_at' => new DateTime('now'), 'updated_at' => new DateTime('now')
            ]
        ]);
    }
}
