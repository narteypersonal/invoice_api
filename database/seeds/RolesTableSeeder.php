<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            [
                'name' => 'admin',
                'created_at' => new DateTime('now'), 'updated_at' => new DateTime('now')
            ],
            [
                'name' => 'vendor',
                'created_at' => new DateTime('now'), 'updated_at' => new DateTime('now')
            ],
            [
                'name' => 'clerk',
                'created_at' => new DateTime('now'), 'updated_at' => new DateTime('now')
            ]
        ]);
    }
}
