<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_number', function (Blueprint $table) {
            $table->increments('id');
            $table->string('prefix', 6)->default('exp');
            $table->integer('alpha_index')->unsigned();
            $table->integer('number')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->dateTime('deleted_at')->nullable();
            $table->dateTime('updated_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('created_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->foreign("app_id")->references("id")->on("apps");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_number');
    }
}
