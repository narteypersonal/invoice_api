<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_number', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->unsigned()->default(1);
            $table->string('prefix', 6)->default('CRD');
            $table->integer('alpha_index')->unsigned()->default(1);
            $table->integer('app_id')->unsigned();
            $table->dateTime('created_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('updated_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->foreign("app_id")->references("id")->on("apps");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_number');
    }
}
