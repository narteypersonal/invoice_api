<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtorsLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debtors_ledger', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_no', 15)->unique();
            $table->double('total_sale', 0);
            $table->double('amount_paid', 0);
            $table->double('balance_bd', 0);
            $table->boolean('is_void')->default(false)->nullable();
            $table->longtext('void_reason')->nullable();
            $table->integer('replace_void')->unsigned()->nullable();
            $table->dateTime('created_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('updated_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->integer('app_id')->unsigned();
            $table->integer('staff_id')->unsigned();
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debtors_ledger');
    }
}
