<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceNumberingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_number', function (Blueprint $table) {
            $table->increments("id");
            $table->string('prefix', 3);
            $table->integer('alpha_index')->default(1);
            $table->integer('number')->default(001);
            $table->dateTime('created_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('updated_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('deleted_at')->nullable();
            $table->integer('app_id')->unsigned();
            $table->foreign("app_id")->references("id")->on("apps");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_numbering');
    }
}
