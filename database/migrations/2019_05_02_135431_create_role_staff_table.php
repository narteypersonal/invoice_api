<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->integer('staff_id')->unsigned();
            $table->dateTime('created_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('updated_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->integer('app_id')->unsigned();
            $table->foreign("role_id")->references("id")->on("roles")->onDelete("cascade");
            $table->foreign("staff_id")->references("id")->on("staffs")->onDelete("cascade");
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_staff');
    }
}
