<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesJournalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_journal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id')->unsigned()->unique();
            $table->double('arears', 0);
            $table->boolean('is_void')->default(false)->nullable();
            $table->longtext('void_reason')->nullable();
            $table->integer('replace_void')->unsigned()->nullable();
            $table->dateTime('created_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('updated_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('app_id')->unsigned();
            $table->integer('staff_id')->unsigned();
            $table->foreign("invoice_id")->references("id")->on("invoices");
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_journal');
    }
}
