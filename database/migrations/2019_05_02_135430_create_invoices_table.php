<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name');
            $table->string('customer_email')->nullable();
            $table->string('customer_phone', 15);
            $table->string('customer_no')->unique();
            $table->string('num', 12)->unique();
            $table->double('gross_total');
            $table->double('discount_total');
            $table->double('tax_total');
            $table->double('discount_rate');
            $table->double('tax_rate');
            $table->double('net_total');
            $table->double('amount_recieved')->default(0);
            $table->double('balance');
            $table->boolean('receipt_trigger')->nullable()->default(false);
            $table->dateTime('created_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('updated_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('deleted_at')->nullable();
            $table->boolean('is_void')->nullable()->default(false);
            $table->integer('app_id')->unsigned();
            $table->integer('staff_id')->unsigned();
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
            $table->foreign("staff_id")->references("id")->on("staffs")->onDelete("cascade");
        });

        // DB::unprepared("
        //     CREATE TRIGGER `AFTER_invoices_INSERT` AFTER INSERT ON `invoices` FOR EACH ROW BEGIN
        //         SELECT number,alpha_index INTO @num, @index FROM invoice_number WHERE app_id = NEW.app_id;

        //         IF(@num = 100) THEN
        //             SET @index = MOD(@index, 26 )+ 1;
        //             SET @num = 1;
        //         ELSE
        //             SET @num = @num + 1;
        //         END IF;
        //         UPDATE invoice_number SET number = @num, alpha_index = @index WHERE app_id = NEW.app_id;
        //     END
        // ");

        DB::unprepared("
            DROP TRIGGER IF EXISTS `AFTER_INVOICES_INSERT`;CREATE TRIGGER `AFTER_INVOICES_INSERT` AFTER INSERT ON `invoices` FOR EACH ROW BEGIN
                SELECT number,alpha_index INTO @num, @index FROM invoice_number WHERE app_id = NEW.app_id;
                #update invoice number
                IF(@num = 100) THEN
                    SET @index = MOD(@index, 26 )+ 1;
                    SET @num = 1;
                ELSE
                    SET @num = @num + 1;
                END IF;
                
                UPDATE invoice_number SET number = @num, alpha_index = @index WHERE app_id = NEW.app_id;

                #insert invoice record into sales account

                INSERT INTO sales_account (invoice_id,sales_total,app_id,staff_id) VALUES (New.id,New.net_total, New.app_id, New.staff_id);
                
                INSERT INTO cashbooks (folio_id,folio_name,folio_number,description,debit,app_id) VALUES (New.id,'Invoice',New.num,'sales invoice',New.amount_recieved, New.app_id);

                #if amount received is less than net total, insert invoice record into sales journal
                IF(New.net_total > New.amount_recieved) THEN
                    INSERT INTO sales_journal (invoice_id,arears,app_id,staff_id) VALUES (New.id, New.balance, New.app_id,New.staff_id);

                    SELECT total_sale,balance_bd,amount_paid INTO @total_sale,@balance_bd,@amount_paid FROM debtors_ledger WHERE customer_no = New.customer_no;
                    IF @total_sale > 0 Then
                    UPDATE debtors_ledger SET total_sale =@total_sale+New.net_total, amount_paid =@amount_paid+New.amount_recieved, balance_bd = @total_sale+New.net_total-@amount_paid+New.amount_recieved WHERE app_id = New.app_id AND  customer_no = New.customer_no;
                ELSE
                    INSERT INTO debtors_ledger (total_sale, customer_no, amount_paid, balance_bd,app_id,staff_id) VALUES (New.net_total,New.customer_no,New.amount_recieved, New.net_total-New.amount_recieved, New.app_id,New.staff_id
                );
                END IF;
            END IF;
            END
        ");

        DB::unprepared("
            DROP TRIGGER IF EXISTS `AFTER_INVOICE_UPDATE`;CREATE TRIGGER `AFTER_INVOICE_UPDATE` AFTER UPDATE ON `invoices` FOR EACH ROW BEGIN
				#if invoice is_void is set to 1
                IF NEW.is_void != OLD.is_void AND New.is_void = 1 THEN

                	#set all receipts of the invoice is_void to 1
                    UPDATE receipts SET is_void = 1, invoice_trigger = 1 WHERE receipts.invoice_id = New.id AND receipts.app_id = New.app_id;
                    
                    #set all invoice_products table records is_void to 1
                    UPDATE invoice_product SET is_void = 1 WHERE invoice_product.invoice_id = New.id AND invoice_product.app_id = New.app_id;

					#set all sales_account of the invoice is_void to 1
                    UPDATE sales_account SET is_void = 1 WHERE sales_account.invoice_id = New.id AND sales_account.app_id = New.app_id;
                    
                    INSERT INTO cashbooks (folio_id,folio_name,folio_number,description,credit,app_id) VALUES (New.id,'Invoice',New.num,'sales invoice revert - autocorrect',New.amount_recieved, New.app_id);

					#set all sales_journal of the invoice is_void to 1
                    UPDATE sales_journal SET sales_journal.is_void = 1 WHERE sales_journal.invoice_id = New.app_id;

                    SELECT total_sale,balance_bd,amount_paid INTO @total_sale,@balance_bd,@amount_paid FROM debtors_ledger WHERE customer_no = New.customer_no;
                    IF @total_sale > 0 Then
                        UPDATE debtors_ledger SET total_sale = @total_sale-New.net_total, amount_paid =@amount_paid-New.amount_recieved, balance_bd = @total_sale-New.net_total-@amount_paid-New.amount_recieved WHERE app_id = New.app_id AND  customer_no = New.customer_no;
                        END IF;

                #if invoice is_void is set to 0
                ELSEIF New.is_void = 0 AND OLD.is_void = 1 THEN

                	#set all sales_account of the invoice is_void to 1
                 	UPDATE sales_account SET is_void = 0 WHERE sales_account.invoice_id = New.id AND sales_account.app_id = New.app_id;
                    
                    INSERT INTO cashbooks (folio_id,folio_name,folio_number,description,debit,app_id) VALUES (New.id,'Invoice',New.num,'sales invoice - autocorrect',New.amount_recieved, New.app_id);

                    #set all sales_journal of the invoice is_void to 1
                    UPDATE sales_journal SET sales_journal.is_void = 0 WHERE sales_journal.invoice_id = New.app_id;
                    
                    #set all invoice_products table records is_void to 0
                    UPDATE invoice_product SET is_void = 0 WHERE invoice_product.invoice_id = New.id AND invoice_product.app_id = New.app_id;

                    #select and update debtors ledger
                    SELECT total_sale,balance_bd,amount_paid INTO @total_sale,@balance_bd,@amount_paid FROM debtors_ledger WHERE customer_no = New.customer_no;
                        UPDATE debtors_ledger SET total_sale = @total_sale+New.net_total, amount_paid =@amount_paid+New.amount_recieved, balance_bd = @total_sale+New.net_total-@amount_paid+New.amount_recieved WHERE app_id = New.app_id And customer_no=New.customer_no;


                END IF;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
