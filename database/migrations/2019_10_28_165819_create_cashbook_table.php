<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashbookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('description');
            $table->integer('folio_id')->unsigned();
            $table->string('folio_name', 100);
            $table->string('folio_number', 20);
            $table->double('debit')->default(0);
            $table->double('credit')->default(0);
            $table->double('balance_bd');
            $table->integer('app_id')->unsigned();
            $table->dateTime('created_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('updated_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
        });

        DB::unprepared("
            DROP TRIGGER IF EXISTS `BEFORE_CASHBOOKS_INSERT`;CREATE TRIGGER `BEFORE_CASHBOOKS_INSERT` BEFORE INSERT ON `cashbooks` FOR EACH ROW BEGIN
                Select cashbooks.balance_bd INTO @bal FROM cashbooks WHERE app_id = New.app_id ORDER BY id DESC LIMIT 1 ;

                #get the absoulet value of balanace_bd
                SET @absBal = ABS(@bal);
                IF NEW.credit > 0 AND @absBal > 0 THEN
                    SET New.balance_bd = @bal - New.credit;
                ELSEIF NEW.credit THEN
                    SET New.balance_bd = 0 - New.credit;
                ELSEIF NEW.debit > 0 AND @absBal > 0 THEN
                    SET New.balance_bd = @bal + New.debit;
                ELSEIF NEW.debit > 0 THEN
                    SET New.balance_bd = 0 + New.debit;
                END IF;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashbook');
    }
}
