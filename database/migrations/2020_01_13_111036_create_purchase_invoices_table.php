<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseInvoicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchase_invoices', function (Blueprint $table) {
			$table->string('number',15);
			$table->double('amount');
			$table->double('balance_bd');
			$table->integer('app_id');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('deleted_at');
			$table->double('carriage_cost');
			$table->boolean('is_serviced');
			$table->integer('staff_id');
			$table->integer('creditor_id');
			$table->string('name',255);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('purchase_invoices');
	}

}
