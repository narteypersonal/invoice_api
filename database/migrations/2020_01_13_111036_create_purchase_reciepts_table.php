<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRecieptsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchase_reciepts', function (Blueprint $table) {
			$table->string('number',15);
			$table->double('amount');
			$table->double('amount_paid');
			$table->double('amount_changed');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
			$table->dateTime('deleted_at');
			$table->boolean('is_void');
			$table->integer('app_id');
			$table->integer('purchase_invoice_id');
			$table->integer('staff_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('purchase_reciepts');
	}

}
