<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->boolean('is_void')->nullable()->default(false);
            $table->double('quantity')->unsigned()->nullable();
            $table->dateTime('created_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('updated_at')->nullable()->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('deleted_at')->nullable();
            $table->integer('staff_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
            $table->foreign("product_id")->references("id")->on("products")->onDelete("cascade");
        });

        DB::unprepared("
            DROP TRIGGER IF EXISTS `AFTER_STOCK_INSERT`;CREATE TRIGGER `AFTER_STOCK_INSERT` AFTER INSERT ON `stocks` FOR EACH ROW BEGIN
                #add stock quantity to product quantity
                UPDATE products SET quantity = (New.quantity + products.quantity) WHERE products.id = New.product_id AND products.app_id = New.app_id;

            END
        ");
        
        DB::unprepared("
            DROP TRIGGER IF EXISTS `AFTER_STOCK_UPDATE`;CREATE TRIGGER `AFTER_STOCK_UPDATE` AFTER UPDATE ON `stocks` FOR EACH ROW BEGIN
                IF NEW.is_void != OLD.is_void AND New.is_void = 1 THEN
                #substract stock quantity to product quantity
                UPDATE products SET quantity = (products.quantity - New.quantity) WHERE products.id = New.product_id AND products.app_id = New.app_id;
                ELSEIF New.is_void = 0 AND OLD.is_void = 1 THEN
                    #add stock quantity to product quantity
                    UPDATE products SET quantity = (New.quantity + products.quantity) WHERE products.id = New.product_id AND products.app_id = New.app_id;
                END IF;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
