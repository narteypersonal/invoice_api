<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_product', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('invoice_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->double('quantity');
            $table->double('unit_price');
            $table->double('tax_rate');
            $table->double('discount_rate');
            $table->longtext('description')->nullable();
            $table->double('sub_total');
            $table->boolean('is_void')->nullable()->default(false); 
            $table->dateTime('created_at')->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->dateTime('updated_at')->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->integer('staff_id');
            $table->integer('app_id')->unsigned();
            $table->foreign("invoice_id")->references("id")->on("invoices")->onDelete("cascade");
            $table->foreign("product_id")->references("id")->on("products")->onDelete("cascade");
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
        });


        DB::unprepared("
            DROP TRIGGER IF EXISTS `AFTER_INVOICE_PRODUCT_INSERT`;CREATE TRIGGER `AFTER_INVOICE_PRODUCT_INSERT` AFTER INSERT ON `invoice_product` FOR EACH ROW BEGIN
                #substract stock quantity to product quantity
                UPDATE products SET quantity = (products.quantity - NEW.quantity), quantity_sold = (products.quantity_sold + NEW.quantity) WHERE products.id = NEW.product_id AND products.app_id = NEW.app_id;
            END
        ");
        
        DB::unprepared("
            DROP TRIGGER IF EXISTS `AFTER_INVOICE_PRODUCT_UPDATE`;CREATE TRIGGER `AFTER_INVOICE_PRODUCT_UPDATE` AFTER UPDATE ON `invoice_product` FOR EACH ROW BEGIN
                IF NEW.is_void != OLD.is_void AND NEW.is_void = 1 THEN
                    #add stock quantity to product quantity
                    UPDATE products SET quantity = (NEW.quantity + products.quantity), quantity_sold = (products.quantity_sold - NEW.quantity) WHERE products.id = NEW.product_id AND products.app_id = NEW.app_id;
                ELSEIF NEW.is_void = 0 AND OLD.is_void = 1 THEN
                    #substract stock quantity to product quantity
                    UPDATE products SET quantity = (products.quantity - NEW.quantity), quantity_sold = (products.quantity_sold + NEW.quantity) WHERE products.id = NEW.product_id AND products.app_id = NEW.app_id;
                END IF;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_product');
    }
}
