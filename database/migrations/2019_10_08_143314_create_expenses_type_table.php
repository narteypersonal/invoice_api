<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->dateTime('created_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('updated_at')->default(DB::raw("CURRENT_TIMESTAMP"))->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('app_id')->unsigned();
            $table->longtext('description')->nullable();
            $table->foreign("app_id")->references("id")->on("apps")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_type');
    }
}
